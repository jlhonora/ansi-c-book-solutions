/*
 *  3d5.h
 *  CProgrammingLanguage
 *
 *  Created by Jose Luis Honorato on 2/28/12.
 *  Copyright 2012 PUC. All rights reserved.
 *
 */

void itob(int n, char s[], int base);
void reverse(char s[]);