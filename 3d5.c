/*
 *  3d5.c
 *  CProgrammingLanguage
 *
 *  Created by Jose Luis Honorato on 2/28/12.
 *  Copyright 2012 PUC. All rights reserved.
 *
 */
#include <stdlib.h>
#include <string.h>
#include "3d5.h"

// Transforms an integer to a string of certain base
void itob(int n, char s[], int base) {
	int i, sign;
	if((sign = n) < 0) 
		n = -n;
	do {
		s[i++] = n % base + (n % base <= 10 ? '0' : 'A');
	} while ((n = n / base) > 0);
	if (sign < 0)
		s[i++] = '-';
	s[i] = '\0';
	reverse(s);
}

void reverse(char s[]) {
	int len, len_half, i;
	char temp1;
	len = strlen((const char *) s);
	len_half = (int) len / 2;
	for (i = 0; i < len_half; i++) {
		temp1 = s[i];
		s[i] = s[len - i - 1];
		s[len - i - 1] = temp1;
	}
		
}
